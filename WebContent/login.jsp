<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="com.qualcomm.isrm.appsec.*,java.io.*,java.util.*"%>
    <%
    if (request.getParameter("name")!=null) {
    	User u=new User(request.getParameter("name"));
    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
    	ObjectOutput oout = null;
    	try {
    	  oout = new ObjectOutputStream(bos);   
    	  oout.writeObject(u);
    	  byte[] bytes = bos.toByteArray();
    	  Cookie c=new Cookie("user",Base64.getEncoder().encodeToString(bytes));
    	  response.addCookie(c);
          response.sendRedirect("index.jsp");
    	} finally {
    	  try {
    	    if (oout != null) {
    	      oout.close();
    	    }
    	  } catch (IOException ex) {
    	    // ignore close exception
    	  }
    	  try {
    	    bos.close();
    	  } catch (IOException ex) {
    	    // ignore close exception
    	  }
    	}
    } else {
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
</head>
<body>
<form action="login.jsp" method="POST">
<input type="text" name="name">
<input type="submit">
</form>
</body>
</html>
<%
}
%>