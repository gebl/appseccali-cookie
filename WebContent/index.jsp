<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="com.qualcomm.isrm.appsec.*,java.io.*,java.util.*"%>
<%
User u=null;
Cookie cookie = null;
Cookie[] cookies = null;
// Get an array of Cookies associated with this domain
cookies = request.getCookies();
if( cookies != null ){
   for (int i = 0; i < cookies.length; i++){
      cookie = cookies[i];
      if (cookie.getName().equals("user")) {
    	  ByteArrayInputStream bis = new ByteArrayInputStream(Base64.getDecoder().decode(cookie.getValue()));
    	  ObjectInput in = null;
    	  try {
    	    in = new ObjectInputStream(bis);
    	    u=(User)in.readObject(); 
    	  } finally {
    	    try {
    	      bis.close();
    	    } catch (IOException ex) {
    	      // ignore close exception
    	    }
    	    try {
    	      if (in != null) {
    	        in.close();
    	      }
    	    } catch (IOException ex) {
    	      // ignore close exception
    	    }
    	  }
      }
   }
}

if (u!=null) {
	%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>This is an app.</title>
</head>
<body>
<h1>Logged in!</h1>
<p>Hello <%=u.getName()%> you <%
if (u.isAdmin()) {
	out.write("are");
} else {
	out.write("aren't");
}
%> an admin user.</p>
</body>
</html><%
} else {
	response.sendRedirect("login.jsp");
}
%>

