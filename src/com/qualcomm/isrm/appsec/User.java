package com.qualcomm.isrm.appsec;

import java.io.Serializable;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private boolean userIsAdmin=false;
	
	
	public User(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public boolean isAdmin(){
		return this.userIsAdmin;
	}
}
